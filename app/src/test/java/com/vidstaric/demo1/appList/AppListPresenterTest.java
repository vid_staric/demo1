package com.vidstaric.demo1.appList;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vid on 3/15/18.
 */
public class AppListPresenterTest {

    @Mock
    private AppListContract.View mView;

    private AppListPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mPresenter = new AppListPresenter(mView);
    }

    @Test
    public void onLoad_noItems() throws Exception {
        when(mView.getPackageInfoList()).thenReturn(new ArrayList<>());

        mPresenter.onLoad();

        verify(mView).setListViewItems(argThat(List::isEmpty));
        verify(mView).showNoOutfitAppsInstalled();
    }

    @Test
    public void onLoad_noOutfitItems() throws Exception {
        List<ApplicationInfo> packageInfoList = Arrays.asList(
                getPackageInfoItemWith("com.test.1"),
                getPackageInfoItemWith("com.test.2"));

        when(mView.getPackageInfoList()).thenReturn(packageInfoList);

        mPresenter.onLoad();

        verify(mView).setListViewItems(argThat(List::isEmpty));
        verify(mView).showNoOutfitAppsInstalled();
    }

    @Test
    public void onLoad_OutfitItems() throws Exception {
        List<ApplicationInfo> packageInfoList = Arrays.asList(
                getPackageInfoItemWith("com.test.1"),
                getPackageInfoItemWith("com.test.2"),
                getPackageInfoItemWith("com.outfit7.test"));

        when(mView.getPackageInfoList()).thenReturn(packageInfoList);

        mPresenter.onLoad();

        verify(mView).setListViewItems(argThat(argument -> argument.size() == 1 && argument.get(0).packageName.equals("com.outfit7.test")));
        verify(mView, never()).showNoOutfitAppsInstalled();
    }


    private ApplicationInfo getPackageInfoItemWith(String packageName) {
        ApplicationInfo packageInfo = new ApplicationInfo();
        packageInfo.packageName = packageName;

        return packageInfo;
    }
}