package com.vidstaric.demo1.appList;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidstaric.demo1.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vid on 3/15/18.
 */

public class AppListAdapter extends BaseAdapter {

    private List<ApplicationInfo> mItemList;
    private PackageManager mPackageManager;

    public AppListAdapter(PackageManager mPackageManager) {
        this.mPackageManager = mPackageManager;
        this.mItemList = new ArrayList<>();
    }

    public void displayItems(@NonNull List<ApplicationInfo> itemList) {
        mItemList = itemList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public ApplicationInfo getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.view_app_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        ApplicationInfo info = getItem(position);
        holder.icon.setImageDrawable(info.loadIcon(mPackageManager));
        holder.name.setText(info.loadLabel(mPackageManager));

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.name)
        TextView name;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
