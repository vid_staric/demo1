package com.vidstaric.demo1.appList;

import android.content.pm.ApplicationInfo;

import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by vid on 3/15/18.
 */

public class AppListPresenter implements AppListContract.Presenter {

    private AppListContract.View mView;

    public AppListPresenter(AppListContract.View view) {
        this.mView = view;

        mView.setPresenter(this);
    }

    @Override
    public void onLoad() {
        List<ApplicationInfo> infoList = StreamSupport.stream(mView.getPackageInfoList())
                .filter(packageInfo -> packageInfo.packageName.contains("com.outfit7."))
                .collect(Collectors.toList());

        if (infoList.isEmpty())
            mView.showNoOutfitAppsInstalled();

        mView.setListViewItems(infoList);
    }
}
