package com.vidstaric.demo1.appList;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import com.vidstaric.demo1.R;
import com.vidstaric.demo1.appDetails.AppDetailsActivity;
import com.vidstaric.demo1.brodcastReciver.AppInstallsObserver;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vid on 3/15/18.
 */

public class AppListActivity extends AppCompatActivity implements AppListContract.View, Observer {

    @BindView(R.id.app_list)
    ListView mListView;

    private AppListAdapter mAdapter;
    private AppListContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new AppListPresenter(this);

        setContentView(R.layout.activity_app_list);

        ButterKnife.bind(this);

        mAdapter = new AppListAdapter(getPackageManager());
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener((parent, view, position, id) -> {
            ApplicationInfo info = mAdapter.getItem(position);

            Intent intent = new Intent(this, AppDetailsActivity.class);
            intent.putExtra(AppDetailsActivity.EXTRA_APP_PACKAGE, info.packageName);

            startActivity(intent);
        });

        mPresenter.onLoad();
        AppInstallsObserver.getInstance().addObserver(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppInstallsObserver.getInstance().deleteObserver(this);
    }

    @Override
    public void setPresenter(AppListContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public List<ApplicationInfo> getPackageInfoList() {
        return getPackageManager().getInstalledApplications(0);
    }

    @Override
    public void setListViewItems(List<ApplicationInfo> packageInfoList) {
        mAdapter.displayItems(packageInfoList);
    }

    @Override
    public void showNoOutfitAppsInstalled() {
        Toast.makeText(this, "No Outfit7 apps installed on this device", Toast.LENGTH_LONG).show();
    }

    @Override
    public void update(Observable o, Object arg) {
        mPresenter.onLoad();
    }
}
