package com.vidstaric.demo1.appList;

import android.content.pm.ApplicationInfo;

import com.vidstaric.demo1.BasePresenter;
import com.vidstaric.demo1.BaseView;

import java.util.List;

/**
 * Created by vid on 3/15/18.
 */

public interface AppListContract {

    interface View extends BaseView<Presenter> {

        List<ApplicationInfo> getPackageInfoList();

        void setListViewItems(List<ApplicationInfo> packageInfoList);

        void showNoOutfitAppsInstalled();
    }

    interface Presenter extends BasePresenter {

    }
}
