package com.vidstaric.demo1;

/**
 * Created by vid on 3/15/18.
 */

public interface BasePresenter {
    void onLoad();
}
