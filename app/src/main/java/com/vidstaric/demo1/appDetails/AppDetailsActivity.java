package com.vidstaric.demo1.appDetails;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidstaric.demo1.R;
import com.vidstaric.demo1.brodcastReciver.AppInstallsObserver;

import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vid on 3/15/18.
 */

public class AppDetailsActivity extends AppCompatActivity implements Observer {

    public static final String EXTRA_APP_PACKAGE = "APP_PACKAGE";

    @BindView(R.id.icon)
    ImageView mIcon;
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.package_name)
    TextView mPackageName;
    @BindView(R.id.version_code)
    TextView mVersionCode;
    @BindView(R.id.version_name)
    TextView mVersionName;

    private String mAppPackageName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activtiy_app_details);

        ButterKnife.bind(this);

        mAppPackageName = getIntent().getStringExtra(EXTRA_APP_PACKAGE);

        loadPackageInfo();

        AppInstallsObserver.getInstance().addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        loadPackageInfo();
    }

    private void loadPackageInfo() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(mAppPackageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (info != null)
            setUpDisplay(info);
        else
            showAppIsMissingError();
    }

    @OnClick(R.id.open_app)
    public void onOpenApp() {
        Intent intent = getPackageManager().getLaunchIntentForPackage(mAppPackageName);
        if (intent != null)
            startActivity(intent);
        else
            showAppIsMissingError();
    }

    private void showAppIsMissingError() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.oops)
                .setMessage(R.string.app_is_gone)
                .setPositiveButton(R.string.go_back, (dialog, which) -> finish())
                .setCancelable(false)
                .show();
    }

    private void setUpDisplay(@NonNull PackageInfo info) {
        CharSequence appName = info.applicationInfo.loadLabel(getPackageManager());

        setTitle(appName);
        mIcon.setImageDrawable(info.applicationInfo.loadIcon(getPackageManager()));
        mName.setText(appName);
        mPackageName.setText(info.packageName);
        mVersionCode.setText(String.valueOf(info.versionCode));
        mVersionName.setText(info.versionName);
    }
}
