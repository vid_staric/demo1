package com.vidstaric.demo1;

/**
 * Created by vid on 3/15/18.
 */

public interface BaseView<PRESENTER> {
    void setPresenter(PRESENTER presenter);
}
