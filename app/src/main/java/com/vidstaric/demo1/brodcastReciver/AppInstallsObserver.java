package com.vidstaric.demo1.brodcastReciver;

import java.util.Observable;

/**
 * Created by vid on 3/15/18.
 */

public class AppInstallsObserver extends Observable {

    private static AppInstallsObserver INSTANCE = new AppInstallsObserver();

    public static AppInstallsObserver getInstance() {
        return INSTANCE;
    }

    private AppInstallsObserver() {
    }

    void onInstalledAppsChanged() {
        setChanged();
        notifyObservers();
    }
}
